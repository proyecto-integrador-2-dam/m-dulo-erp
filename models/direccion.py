# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_direccion(models.Model):
    _name = 'batoi_logic.direccion'

    cliente = fields.Many2one('batoi_logic.cliente', 'corresponde')
    calle = fields.Char('Calle', size=200, required=True)
    cp = fields.Many2one('batoi_logic.cp', 'Codigo Postal')
    _sql_constraints = [
        ('rental_proveedor_cliente_id_uniq',
         'UNIQUE (cliente,id)',
         'Esta direccion ya esta asignada a este cliente.')]
