# -*- coding: utf-8 -*-

from odoo import models, fields, api
class batoi_logic_ruta(models.Model):
     _name = 'batoi_logic.ruta'

     repartidor = fields.Many2one('batoi_logic.repartidor', 'Repartidor asignado', required=True)
