# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_cp(models.Model):
    _name = 'batoi_logic.cp'
    _rec_name = 'cp'
    cp = fields.Integer('Codigo postal', size=5, required=True)
    ciudad = fields.Many2one('batoi_logic.ciudad', 'Ciudad a la que pertenece')
    _sql_constraints = [
        ('rental_proveedor_cp_uniq',
         'UNIQUE (cp)',
         'Este Codifo Postal ya se encuentra registrado.')]

