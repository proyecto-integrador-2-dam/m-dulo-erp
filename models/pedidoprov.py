# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_pedidoprov(models.Model):
    _name = 'batoi_logic.pedidoprov'

    producto = fields.Many2one('batoi_logic.producto', 'Producto', required=True)
    proveedor = fields.Many2one('batoi_logic.proveedor', 'Proveedor', required=True)
    cantidad = fields.Integer('Cantidad', required=True)
    fecha = fields.Date('Fecha', required=True)
