# -*- coding: utf-8 -*-

from odoo import models, fields, api
class batoi_logic_repartidor(models.Model):
     _name = 'batoi_logic.repartidor'
     _rec_name = 'nombre'
     nombre = fields.Char('Nombre', size=50, required=True)
     passw = fields.Char('Contrasenya', size=40, required=True)
     apellidos = fields.Char('Apellidos', size=100, required=True)
     camion = fields.Many2one('batoi_logic.camion', 'Camion Asignado', required=True)
     _sql_constraints = [
        ('rental_proveedor_camion_uniq',
         'UNIQUE (camion)',
         'Este camion ya se encuentra asignado a un repartidor.')]
