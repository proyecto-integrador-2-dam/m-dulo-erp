# -*- coding: utf-8 -*-

from odoo import models, fields, api
class batoi_logic_albaran(models.Model):
     _name = 'batoi_logic.albaran'
     pedido = fields.Many2one('batoi_logic.pedido', 'corresponde', required=True)
     ruta = fields.Many2one('batoi_logic.ruta', 'asignado', required=True)
     _sql_constraints = [
        ('rental_proveedor_pedido_uniq',
         'UNIQUE (pedido)',
         'Este pedido ya se encuentra asignado a un albaran.')]
