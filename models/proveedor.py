# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_producto(models.Model):
    _name = 'batoi_logic.proveedor'
    _rec_name = 'nombre'
    cif = fields.Char('CIF', size=9, required=True)
    nombre = fields.Char('Nombre Empresa', size=50, required=True)
    email = fields.Char('E-mail', size=60, required=True)
    telefono = fields.Integer('Telefono', size=9)
    _sql_constraints = [
        ('rental_proveedor_cif_uniq',
         'UNIQUE (cif)',
         'Este CIF ya se encuentra registrado.')]

