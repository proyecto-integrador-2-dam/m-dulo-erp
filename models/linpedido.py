# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_linpedido(models.Model):
    _name = 'batoi_logic.linpedido'

    linea = fields.Integer('Linea Pedido', required=True)
    cantidad = fields.Integer('Cantidad', required=True)
    pedido = fields.Many2one('batoi_logic.pedido', 'es de', required=True)
    producto = fields.Many2one('batoi_logic.producto', 'tiene', required=True)
    _sql_constraints = [
        ('rental_proveedor_pedido_linea_uniq',
         'UNIQUE (pedido,linea)',
         'Esta producto ya tiene esa linea asignada')]
