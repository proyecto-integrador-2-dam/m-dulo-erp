# -*- coding: utf-8 -*-

from odoo import models, fields, api
class batoi_logic_ubicacion(models.Model):
     _name = 'batoi_logic.ubicacion'
     fecha = fields.Date('Fecha', required=True)
     latitud = fields.Float('Latitud', required=True)
     longitud = fields.Float('Longitud', required=True)
     repartidor = fields.Many2one('batoi_logic.repartidor', 'Repartidor asignado', required=True)
