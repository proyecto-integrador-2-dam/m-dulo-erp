# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_productoproveedor(models.Model):
    _name = 'batoi_logic.productoproveedor'

    producto = fields.Many2one('batoi_logic.producto', 'Producto')
    proveedor = fields.Many2one('batoi_logic.proveedor', 'Proveedor')
    precio = fields.Float('Precio', required=True)
    _sql_constraints = [
        ('rental_proveedor_producto_uniq',
         'UNIQUE (producto, proveedor)',
         'Este producto ya esta registrado para este proveedor.')]
