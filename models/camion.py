# -*- coding: utf-8 -*-

from odoo import models, fields, api
class batoi_logic_camion(models.Model):
     _name = 'batoi_logic.camion'
     _rec_name = 'matricula'
     matricula = fields.Char('Matricula', size=7, required=True)
     capacidad = fields.Integer('Capacidad', required=True)
     _sql_constraints = [
        ('rental_proveedor_camion_uniq',
         'UNIQUE (matricula)',
         'Este camion ya se encuentra registrado.')]

