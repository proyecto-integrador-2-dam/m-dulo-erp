# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_ciudad(models.Model):
    _name = 'batoi_logic.ciudad'
    _rec_name = 'nombre'
    nombre = fields.Char('Nombre', size=50, required=True)
    provincia = fields.Char('Provinicia', size=50, required=True)
    pais = fields.Char('Pais', size=50, required=True)
