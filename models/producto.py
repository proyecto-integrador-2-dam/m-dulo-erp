# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_producto(models.Model):
    _name = 'batoi_logic.producto'
    _rec_name = 'nombre'
    nombre = fields.Char('Nombre', size=50, required=True)
    descripcion = fields.Char('Descripcion', size=300, required=True)
    precio = fields.Float('PVP')
    stock = fields.Integer('Stock')

