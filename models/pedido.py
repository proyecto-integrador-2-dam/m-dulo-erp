# -*- coding: utf-8 -*-

from odoo import models, fields, api

class batoi_logic_pedido(models.Model):
    _name = 'batoi_logic.pedido'

    estado = fields.Char('Estado', size=50, required=True)
    observaciones = fields.Char('Observaciones', size=300, required=True)
    total = fields.Integer('Total')
    fecha = fields.Date('Fecha Pedido', required=True)
    cliente = fields.Many2one('batoi_logic.cliente', 'Cliente:')
    direccion = fields.Many2one('batoi_logic.direccion', 'Direccion:')
