# -*- coding: utf-8 -*-
{
    'name': "BatoiLogic",

    'summary': """
        Modulo Gestion empresa Batoilogic""",

    'description': """
        Modulo para gestion de diferentes modulos de la base de datos de BatoiLogic
    """,

    'author': "Grupo 3",
    'application': True,
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    
}
